package org.gcube.informationsystem.registry.testsuite.security;

import java.io.FileReader;
import java.io.StringWriter;

import org.apache.axis.message.addressing.Address;
import org.apache.axis.message.addressing.EndpointReferenceType;
import org.gcube.common.core.contexts.GCUBERemotePortTypeContext;
import org.gcube.common.core.contexts.GHNContext;
import org.gcube.common.core.resources.GCUBEGenericResource;
import org.gcube.common.core.resources.GCUBEHostingNode;
import org.gcube.common.core.resources.GCUBEResource;
import org.gcube.common.core.resources.GCUBERunningInstance;
import org.gcube.common.core.resources.GCUBEService;
import org.gcube.common.core.scope.GCUBEScope;
import org.gcube.common.core.security.GCUBESecurityManager;
import org.gcube.informationsystem.registry.stubs.resourceregistration.CreateMessage;
import org.gcube.informationsystem.registry.stubs.resourceregistration.ResourceRegistrationPortType;
import org.gcube.informationsystem.registry.stubs.resourceregistration.service.ResourceRegistrationServiceAddressingLocator;

/**
 * Tester for Registration.create() operation
 * 
 * @author Manuele Simi (CNR-ISTI)
 * 
 */
public class SecureRegistryRegistrationTest {

	/**
	 * 
	 * @param args
	 *            parameters:
	 *            <ul>
	 *            <li>registration portType URI
	 *            <li>resource file
	 *            <li>resource type
	 *            <li>caller scope
	 *            <li>identity
	 * 			</ul>
	 * @throws Exception
	 *             if the registration fails
	 */

	public static void main(String[] args) throws Exception {

		for (String param : args) System.out.println("param "+ param);

		if (args.length != 5) {
			printUsage();
		}


		String identity = args [4];
		
		GCUBEResource resource = getResource(args[2], new FileReader(args[1]));
		EndpointReferenceType factoryEPR = new EndpointReferenceType();
		try {
			factoryEPR.setAddress(new Address(args[0]));
			ResourceRegistrationServiceAddressingLocator locator = new ResourceRegistrationServiceAddressingLocator(); 
			ResourceRegistrationPortType registration = locator.getResourceRegistrationPortTypePort(factoryEPR);
			GCUBESecurityManager securityManager = Utils.generateAndConfigureDefaultSecurityManager(identity, true);
			securityManager.setSecurity(registration, GCUBESecurityManager.AuthMode.PRIVACY, GCUBESecurityManager.DelegationMode.FULL); 
			registration = GCUBERemotePortTypeContext.getProxy(registration, GCUBEScope.getScope(args[3]), securityManager);
			CreateMessage message = new CreateMessage();
			StringWriter writer = new StringWriter();
			resource.store(writer);
			message.setProfile(writer.toString());
			message.setType(resource.getType());
			registration.create(message);
			System.out.println("Resource successfully registered");
			
		} catch (Exception e) {
			e.printStackTrace();
			Runtime.getRuntime().exit(1);
		}



	}

	/**
	 * Loads the specific GCUBEResource class
	 * 
	 * @param type
	 *            the resource type
	 * @param file
	 *            the file representation of the profile
	 * @return the resource class
	 * @throws Exception
	 *             if the loading fails
	 */
	static GCUBEResource getResource(String type, FileReader file)
			throws Exception {
		if (type.compareTo("Service") == 0) {
			GCUBEService service = GHNContext
					.getImplementation(GCUBEService.class);
			service.load(file);
			return (GCUBEResource) service;
		} else if (type.compareTo("RunningInstance") == 0) {
			GCUBERunningInstance instance = GHNContext
					.getImplementation(GCUBERunningInstance.class);
			instance.load(file);
			return (GCUBEResource) instance;
		} else if (type.compareTo("GHN") == 0) {
			GCUBEHostingNode node = GHNContext
					.getImplementation(GCUBEHostingNode.class);
			node.load(file);
			return (GCUBEResource) node;
		} else if (type.compareTo("GenericResource") == 0) {
			GCUBEGenericResource generic = GHNContext
					.getImplementation(GCUBEGenericResource.class);
			generic.load(file);
			return (GCUBEResource) generic;
		}

		throw new Exception(type + " is an invalid resource type");

	}

	/**
	 * Prints tester usage syntax
	 */
	static void printUsage() {
		System.out.println("RegistryRegistrationTest <registration URI> <resource file> <resource type> <caller scope> <identity>");
		System.out.println("allowed types are: RunningInstance/Service/GHN/GenericResource");
		System.exit(1);
	}
}
