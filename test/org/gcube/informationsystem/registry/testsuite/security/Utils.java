package org.gcube.informationsystem.registry.testsuite.security;

import org.gcube.common.core.security.GCUBEClientSecurityManager;
import org.gcube.common.core.security.GCUBESecurityManager;
import org.gcube.common.core.security.GCUBESecurityManagerImpl;
import org.gcube.common.core.security.context.SecurityContextFactory;
import org.gcube.common.core.utils.logging.GCUBEClientLog;

public class Utils {
	public static GCUBESecurityManager generateAndConfigureDefaultSecurityManager (String identity, final boolean isSecurityEnabled)
	{
		GCUBEClientLog logger = new GCUBEClientLog(Utils.class);
		
		GCUBESecurityManager securityManager = null;
		
		if (isSecurityEnabled)
		{
			try {
				securityManager = SecurityContextFactory.getInstance().getSecurityContext().getDefaultSecurityManager();
				logger.debug("Identity = "+identity);
				((GCUBEClientSecurityManager) securityManager).setDefaultIdentityParameter(identity);
			
			} catch (Exception e) {
				logger.error("Unable to generate default security manager: trying without security...",e);
				securityManager = new GCUBESecurityManagerImpl() { 			
					public boolean isSecurityEnabled() {return isSecurityEnabled;}			
				};
			}
		}
		else 
		{

			securityManager = new GCUBESecurityManagerImpl() { 			
				public boolean isSecurityEnabled() {return isSecurityEnabled;}			
			};
		}
		
		return securityManager;
	}

}
