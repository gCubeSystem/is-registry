package org.gcube.informationsystem.registry.testsuite;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.StringWriter;

import org.gcube.common.core.contexts.GHNContext;
import org.gcube.common.core.resources.GCUBEGenericResource;
import org.gcube.common.core.resources.GCUBEHostingNode;
import org.gcube.common.core.resources.GCUBEResource;
import org.gcube.common.core.resources.GCUBERunningInstance;
import org.gcube.common.core.resources.GCUBERuntimeResource;
import org.gcube.common.core.resources.GCUBEService;
import org.gcube.informationsystem.registry.stubs.resourceregistration.CreateMessage;
import org.gcube.informationsystem.registry.stubs.resourceregistration.ResourceRegistrationPortType;

public class Resource {

	private GCUBEResource resource;
	private File source;
	protected Resource (File resource, String type) throws FileNotFoundException,Exception {
		this.source = resource;
		this.resource = this.load(type, new FileReader(resource));
	}
	
	
	/**
	 * Registers the resource in the given portType
	 * @param registration
	 * @param resourceType
	 * @param fileToRegister
	 * @throws Exception
	 */
	protected void register(ResourceRegistrationPortType registration) throws Exception {
		CreateMessage message = new CreateMessage();
		StringWriter writer = new StringWriter();
		resource.store(writer);
		message.setProfile(writer.toString());
		message.setType(resource.getType());
		registration.create(message);
		System.out.println("Resource "+ this.source.getAbsolutePath() +" successuflly registered: " + this.resource.getID());
	}
	/**
	 * Loads the specific GCUBEResource class
	 * 
	 * @param type the resource type
	 * @param file the file representation of the profile
	 * @return the resource class
	 * @throws Exception
	 *             if the loading fails
	 */
	private GCUBEResource load(String type, FileReader file) throws Exception {
		if (type.compareTo("Service") == 0) {
			GCUBEService service = GHNContext.getImplementation(GCUBEService.class);
			service.load(file);
			return (GCUBEResource) service;
		} else if (type.compareTo("RunningInstance") == 0) {
			GCUBERunningInstance instance = GHNContext.getImplementation(GCUBERunningInstance.class);
			instance.load(file);
			return (GCUBEResource) instance;
		} else if (type.compareTo("GHN") == 0) {
			GCUBEHostingNode node = GHNContext.getImplementation(GCUBEHostingNode.class);
			node.load(file);
			return (GCUBEResource) node;
		} else if (type.compareTo("GenericResource") == 0) {
			GCUBEGenericResource generic = GHNContext.getImplementation(GCUBEGenericResource.class);
			generic.load(file);
			return (GCUBEResource) generic;
		} else if (type.compareTo("RuntimeResource") == 0) {
			GCUBERuntimeResource rt = GHNContext.getImplementation(GCUBERuntimeResource.class);
			rt.load(file);
			return (GCUBEResource) rt;
		}

		throw new Exception(type + " is an invalid resource type");
	}
}
