package org.gcube.informationsystem.registry.testsuite;


import org.apache.axis.message.addressing.Address;
import org.apache.axis.message.addressing.EndpointReferenceType;
import org.gcube.common.core.contexts.GCUBERemotePortTypeContext;
import org.gcube.common.core.scope.GCUBEScope;
import org.gcube.common.core.security.GCUBESecurityManagerImpl;
import org.gcube.informationsystem.registry.stubs.resourceregistration.RemoveMessage;
import org.gcube.informationsystem.registry.stubs.resourceregistration.ResourceRegistrationPortType;
import org.gcube.informationsystem.registry.stubs.resourceregistration.service.ResourceRegistrationServiceAddressingLocator;

/**
 * 
 * Tester for Registration.remove() operation
 *
 * @author Manuele Simi (ISTI-CNR)
 *
 */
public class RegistryRemoveTest {

	public static void main(String[] args) throws Exception {


		for (String param : args) System.out.println("param "+ param);
		
		// get the scope and the registration URI
		GCUBEScope scope = GCUBEScope.getScope(args[2]);

		EndpointReferenceType factoryEPR = new EndpointReferenceType();
		try {
			factoryEPR.setAddress(new Address(args[0]));
		} catch (Exception e) {
			e.printStackTrace();
			Runtime.getRuntime().exit(1);
		}

		ResourceRegistrationServiceAddressingLocator locator = new ResourceRegistrationServiceAddressingLocator(); 
		GCUBESecurityManagerImpl managerSec = new GCUBESecurityManagerImpl() {
			public boolean isSecurityEnabled() {
				return false;
			}
		};
		
		
		// load the resource
		try {
			ResourceRegistrationPortType registration = locator.getResourceRegistrationPortTypePort(factoryEPR);
			registration = locator.getResourceRegistrationPortTypePort(factoryEPR);		
			RemoveMessage  message = new RemoveMessage();
			registration = GCUBERemotePortTypeContext.getProxy(registration, scope, managerSec);
			message.setType(args[3]);
			message.setUniqueID(args[1]);
			registration.remove(message);
			System.out.println("Profile with ID " + args[1]	+ "has been succesfully removed");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
