package org.gcube.informationsystem.registry.testsuite;

import java.io.File;


import org.apache.axis.message.addressing.Address;
import org.apache.axis.message.addressing.EndpointReferenceType;
import org.gcube.common.core.contexts.GCUBERemotePortTypeContext;
import org.gcube.common.core.scope.GCUBEScope;
import org.gcube.common.core.security.GCUBESecurityManagerImpl;
import org.gcube.informationsystem.registry.stubs.resourceregistration.ResourceRegistrationPortType;
import org.gcube.informationsystem.registry.stubs.resourceregistration.service.ResourceRegistrationServiceAddressingLocator;

/**
 * Tester for Registration.create() operation
 * 
 * @author Manuele Simi (CNR-ISTI)
 * 
 */
public class RegistryRegistrationTest {

	/**
	 * 
	 * @param args
	 *            parameters:
	 *            <ul>
	 *            <li>registration portType URI
	 *            <li>resource file
	 *            <li>resource type
	 *            <li>caller scope
	 * 			</ul>
	 * @throws Exception
	 *             if the registration fails
	 */

	public static void main(String[] args) throws Exception {

		if (args.length != 4) {
			printUsage();
		}

		for (String param : args) System.out.println("param "+ param);
		
		// get the scope and the REGITRATION URI
		GCUBEScope scope = GCUBEScope.getScope(args[2]);
		
		//create the porttype
		EndpointReferenceType factoryEPR = new EndpointReferenceType();
		ResourceRegistrationServiceAddressingLocator locator = new ResourceRegistrationServiceAddressingLocator(); 
		GCUBESecurityManagerImpl managerSec = new GCUBESecurityManagerImpl() {
			public boolean isSecurityEnabled() {
				return false;
			}
		};
		ResourceRegistrationPortType registration = null;
		try {
			factoryEPR.setAddress(new Address(args[0]));
			registration = locator.getResourceRegistrationPortTypePort(factoryEPR);
			registration = locator.getResourceRegistrationPortTypePort(factoryEPR);
			registration = GCUBERemotePortTypeContext.getProxy(registration, scope, managerSec);

		} catch (Exception e) {
			e.printStackTrace();
			Runtime.getRuntime().exit(1);
		}

		File param = new File(args[1]);
		if (param.isDirectory()) 
			registerDirectory(registration,args[3],param);
		 else {
			   try {
				   Resource resource = new Resource(param, args[3]);
				   resource.register(registration);
		 	   } catch (Exception e) {
				System.err.print("Unable to register " + param.getAbsolutePath());
				e.printStackTrace();
		 	   }
		 }
	}

	/**
	 * Registers all the resources in a directory, recursively if needed
	 * @param registration
	 * @param resourceType
	 * @param dirToProcess
	 */
	static void registerDirectory(ResourceRegistrationPortType registration, String resourceType, File dirToProcess) {
		for (File f : dirToProcess.listFiles()) {
			if (f.isDirectory())
				registerDirectory(registration,resourceType,f);
			else
				try {
					Resource resource = new Resource(f, resourceType);
					   resource.register(registration);
				} catch (Exception e) {
					System.err.print("Unable to register " + f.getAbsolutePath());
					e.printStackTrace();
				}
		}
	}

	/**
	 * Prints tester usage syntax
	 */
	static void printUsage() {
		System.out.println("RegistryRegistrationTest <registration URI> <resource file> <caller scope> <resource type>");
		System.out.println("allowed types are: RunningInstance/Service/GHN/GenericResource");
		System.exit(1);
	}
}
